﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kellner.DTO
{
    public class TableQRDTO
    {
        public int TableId { get; set; }

        public int CompanyId { get; set; }
    }
}