﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kellner.DTO
{
    public class TableGeneratorDTO
    {
        public int CompanyId { get; set; }

        public int NumberOfTables { get; set; }
    }
}