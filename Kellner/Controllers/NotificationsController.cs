﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Kellner.Models;
using System.Web;

namespace Kellner.Controllers
{
    public class NotificationsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //// GET: api/Notifications
        //public IQueryable<UserPlayerId> GetUserPlayerIds()
        //{
        //    return db.UserPlayerIds;
        //}

        //// GET: api/Notifications/5
        //[ResponseType(typeof(UserPlayerId))]
        //public IHttpActionResult GetUserPlayerId(int id)
        //{
        //    UserPlayerId userPlayerId = db.UserPlayerIds.Find(id);
        //    if (userPlayerId == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(userPlayerId);
        //}

        // POST: api/Notifications
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult PostUserPlayerId(NotificationViewModel notificationVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userPlayerId = db.UserPlayerIds.FirstOrDefault(up => up.Username == HttpContext.Current.User.Identity.Name);
            userPlayerId.QueueNotifications = notificationVM.QueueNotifications;
            userPlayerId.SubscribedNotifications = notificationVM.SubscribedNotifications;
            db.Entry(userPlayerId).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //// POST: api/Notifications
        //[ResponseType(typeof(UserPlayerId))]
        //public IHttpActionResult PostUserPlayerId(UserPlayerId userPlayerId)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.UserPlayerIds.Add(userPlayerId);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = userPlayerId.Id }, userPlayerId);
        //}

        //// DELETE: api/Notifications/5
        //[ResponseType(typeof(UserPlayerId))]
        //public IHttpActionResult DeleteUserPlayerId(int id)
        //{
        //    UserPlayerId userPlayerId = db.UserPlayerIds.Find(id);
        //    if (userPlayerId == null)
        //    {
        //        return NotFound();
        //    }

        //    db.UserPlayerIds.Remove(userPlayerId);
        //    db.SaveChanges();

        //    return Ok(userPlayerId);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserPlayerIdExists(int id)
        {
            return db.UserPlayerIds.Count(e => e.Id == id) > 0;
        }
    }
}