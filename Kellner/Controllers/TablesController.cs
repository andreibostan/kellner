﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using Kellner.Models;
using Kellner.DTO;
using QRCoder;
using System.Drawing;
using System.Web.Script.Serialization;
using System.Web;

namespace Kellner.Controllers
{
    public class TablesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Tables
        public IQueryable<Table> GetTables()
        {
            return db.Tables;
        }

        // GET: api/Tables/5
        [ResponseType(typeof(Table))]
        public IHttpActionResult GetTable(int id)
        {
            Table table = db.Tables.Find(id);
            if (table == null)
            {
                return NotFound();
            }

            return Ok(table);
        }

        // PUT: api/Tables/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTable(int id, Table table)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != table.Id)
            {
                return BadRequest();
            }

            db.Entry(table).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TableExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tables
        [ResponseType(typeof(Table))]
        [Authorize]
        public IHttpActionResult PostTable(Table table)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tables.Add(table);
            db.SaveChanges();

            table.QRCode = GenerateQRCode(
                        new TableQRDTO()
                        {
                            TableId = table.Id,
                            CompanyId = table.CompanyId
                        }
                    );
            db.Entry(table).State = EntityState.Modified;
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = table.Id }, table);
        }

        // POST: api/Tables
        [ResponseType(typeof(void))]
        [Route("TableGenerate")]
        [Authorize]
        public IHttpActionResult TableGenerate(TableGeneratorDTO tableGenerator)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int? lastTableNumber = GetLastTableNumber(tableGenerator);

            for (int i = 0; i < tableGenerator.NumberOfTables; i++)
            {
                UpdateTable(tableGenerator, lastTableNumber, i);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }        

        

        // DELETE: api/Tables/5
        [ResponseType(typeof(Table))]
        public IHttpActionResult DeleteTable(int id)
        {
            Table table = db.Tables.Find(id);
            if (table == null)
            {
                return NotFound();
            }

            db.Tables.Remove(table);
            db.SaveChanges();

            return Ok(table);
        }

        private string GenerateQRCode(TableQRDTO tableQR)
        {
            var jsonSerializer = new JavaScriptSerializer();
            QRCodeGenerator qrGenerator = new QRCodeGenerator();

            QRCodeData qrCodeData = qrGenerator.CreateQrCode(jsonSerializer.Serialize(tableQR), QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            qrCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] byteImage = ms.ToArray();
            return Convert.ToBase64String(byteImage); //Get Base64
        }

        private int? GetLastTableNumber(TableGeneratorDTO tableGenerator)
        {
            var lastTableName = db.Tables
                                .Where(t => t.CompanyId == tableGenerator.CompanyId)
                                .OrderByDescending(t => t.Id)
                                .FirstOrDefault()?
                                .Name;

            int? lastTableNumber = lastTableName == null ? (int?)null : int.Parse(lastTableName.Substring(5));
            return lastTableNumber;
        }

        private void UpdateTable(TableGeneratorDTO tableGenerator, int? lastTableNumber, int i)
        {
            var table = new Table();

            table.Name = lastTableNumber != null ?
                "Table " + (i + 1 + lastTableNumber).ToString() :
                table.Name = "Table " + (i + 1).ToString();

            table.CompanyId = tableGenerator.CompanyId;
            db.Tables.Add(table);
            db.SaveChanges();

            table.QRCode = GenerateQRCode(
                    new TableQRDTO()
                    {
                        TableId = table.Id,
                        CompanyId = table.CompanyId
                    }
                );
            db.Entry(table).State = EntityState.Modified;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TableExists(int id)
        {
            return db.Tables.Count(e => e.Id == id) > 0;
        }
    }
}