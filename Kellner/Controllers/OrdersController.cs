﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Kellner.Models;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Web.Script.Serialization;
using System.Text;
using System.IO;

namespace Kellner.Controllers
{
    public class OrdersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Orders
        [Authorize]
        [Route("api/OrdersAll")]
        public IQueryable<Order> GetAllOrders()
        {

            var companyId = db.Users.Where(u => u.UserName == HttpContext.Current.User.Identity.Name)
                .FirstOrDefault()
                .CompanyId;
            return db.Orders.Where(o => o.CompanyId == companyId && o.Active == true);
        }

        // GET: api/Orders
        [Authorize]
        public IQueryable<Order> GetOrders()
        {
            return db.Orders.Where(o => o.Username.ToLower().Trim() == HttpContext.Current.User.Identity.Name.ToLower().Trim()
                                                                    && o.Active == true);
        }

        // GET: api/Orders/5
        [ResponseType(typeof(Order))]
        public IHttpActionResult GetOrder(int id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        // PUT: api/Orders/5
        [ResponseType(typeof(void))]
        [Authorize]
        public IHttpActionResult PutOrder(int id, OrderVM orderVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var order = db.Orders.FirstOrDefault(o => o.Id == id);

            if (orderVM.Unsubscribe == true)
            {
                order.Username = null;
            }
            else
            {
                order.Username = HttpContext.Current.User.Identity.Name;
            }

            db.Entry(order).State = EntityState.Modified;

            var message = "Action confirmed by your waiter!";

            var userToNotify = new List<string>() {
                //db.Orders.FirstOrDefault(op => op.Id == id).OrderPlayer.PlayerId
                (from p in db.Orders
                 join pm in db.OrderPlayers on p.OrderPlayerId equals pm.Id
                                     select new { PlayerId = pm.PlayerId }).FirstOrDefault()
                                     .PlayerId
            };
            order.Action = orderVM.Action + "Confirmed";
            sendNotification(message, userToNotify);
            if(order.Action.ToLower() == "payconfirmed")
            {
                order.Active = false;
            }
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT: api/Orders/5
        [ResponseType(typeof(void))]
        [Route("api/OrdersClient/{id}")]
        public IHttpActionResult PutOrder(int id, OrderVM2 orderVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var orderPlayerId = db.OrderPlayers.OrderByDescending(x => x.Id).FirstOrDefault(op => op.PlayerId == orderVM.PlayerId)?.Id;

            if (orderPlayerId == null)
            {
                return BadRequest();
            }

            var order = db.Orders.OrderByDescending(x => x.Id).FirstOrDefault(o => o.OrderPlayerId == orderPlayerId && o.Active == true);

            if (order == null || order.Id != id)
            {
                return BadRequest();
            }
            order.Action = orderVM.Action;

            db.Entry(order).State = EntityState.Modified;
            var message = "Action requested: " + orderVM.Action + " for table: " + order.TableName;

            var usersToNotify = new List<string>();
            if (order.Username != null)
            {
                usersToNotify = db.UserPlayerIds
                    .Where(up => up.Username == order.Username && up.SubscribedNotifications == true)
                    .Select(x => x.PlayerId)
                    .ToList();
            }
            else
            {
                usersToNotify = (from p in db.Users.Where(u => u.CompanyId == order.CompanyId)
                                     join pm in db.UserPlayerIds.Where(up => up.QueueNotifications == true) on p.UserName equals pm.Username
                                     select new { PlayerId = pm.PlayerId })
                                .Select(x => x.PlayerId)
                                .ToList();
            }

            sendNotification(message, usersToNotify);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;                
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Orders
        [ResponseType(typeof(Order))]
        public IHttpActionResult PostOrder(OrderViewModel orderVM)
        {
            var order = new Order()
            {
                CompanyId = orderVM.CompanyId,
                TableId = orderVM.TableId,
                Username = orderVM.Username,
                Active = true,
                Action = orderVM.Action,
                TableName = orderVM.TableName
            };

            var orderPlayer = new OrderPlayer()
            {
                PlayerId = orderVM.PlayerId
            };

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var table = db.Tables.Find(order.TableId);
            var company = db.Companies.Find(order.CompanyId);

            if (table == null || company == null)
                return BadRequest();


            db.OrderPlayers.Add(orderPlayer);
            db.SaveChanges();

            order.TableName = table.Name;
            order.OrderPlayerId = orderPlayer.Id;
            db.Orders.Add(order);

            db.SaveChanges();

            var usersToNotify = (from p in db.Users.Where(u => u.CompanyId == order.CompanyId)
                                join pm in db.UserPlayerIds.Where(up => up.QueueNotifications == true) on p.UserName equals pm.Username
                                select new { PlayerId = pm.PlayerId })
                                .Select(x => x.PlayerId)
                                .ToList();
            var message = "New order in queue. Table name: " + order.TableName;

            sendNotification(message, usersToNotify);

            return CreatedAtRoute("DefaultApi", new { id = order.Id }, order);
        }

        // DELETE: api/Orders/5
        [ResponseType(typeof(Order))]
        public IHttpActionResult DeleteOrder(int id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return NotFound();
            }

            db.Orders.Remove(order);
            db.SaveChanges();

            return Ok(order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return db.Orders.Count(e => e.Id == id) > 0;
        }

        private void sendNotification(string message, List<string> playerIds)
        {
            var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            request.Headers.Add("authorization", "Basic ODA2OGM3NmMtMjY0ZC00Y2FkLThlMDEtYTJiNzFmYmU1NGU4");

            var serializer = new JavaScriptSerializer();
            var obj = new
            {
                app_id = "d6e1d66e-ba7e-4238-90a3-d5733d39e4c5",
                contents = new { en = message },
                include_player_ids = playerIds
            };
            
            var param = serializer.Serialize(obj);
            SendNotificationToOnesignal(request, param);
        }

        private static void SendNotificationToOnesignal(HttpWebRequest request, string param)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(param);

            string responseContent = null;

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }
        }
    }
}