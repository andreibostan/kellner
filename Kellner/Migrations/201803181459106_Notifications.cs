namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Notifications : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserPlayerIds", "QueueNotifications", c => c.Boolean());
            AddColumn("dbo.UserPlayerIds", "SubscribedNotifications", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserPlayerIds", "SubscribedNotifications");
            DropColumn("dbo.UserPlayerIds", "QueueNotifications");
        }
    }
}
