namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userPlayerId : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserPlayerIds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        PlayerId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserPlayerIds");
        }
    }
}
