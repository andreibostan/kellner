namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeeCompany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "CompanyId", c => c.Int());
            AddColumn("dbo.AspNetUsers", "IsEmployee", c => c.Boolean(nullable: false));
            CreateIndex("dbo.AspNetUsers", "CompanyId");
            AddForeignKey("dbo.AspNetUsers", "CompanyId", "dbo.Companies", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "CompanyId", "dbo.Companies");
            DropIndex("dbo.AspNetUsers", new[] { "CompanyId" });
            DropColumn("dbo.AspNetUsers", "IsEmployee");
            DropColumn("dbo.AspNetUsers", "CompanyId");
        }
    }
}
