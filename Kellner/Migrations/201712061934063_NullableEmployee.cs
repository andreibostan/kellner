namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableEmployee : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "IsEmployee", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "IsEmployee", c => c.Boolean(nullable: false));
        }
    }
}
