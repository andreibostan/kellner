// <auto-generated />
namespace Kellner.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class userPlayerId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(userPlayerId));
        
        string IMigrationMetadata.Id
        {
            get { return "201802251715349_userPlayerId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
