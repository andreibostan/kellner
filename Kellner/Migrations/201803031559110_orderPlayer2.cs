namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderPlayer2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderPlayers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        PlayerId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Orders", "OrderPlayerId", c => c.Int());
            CreateIndex("dbo.Orders", "OrderPlayerId");
            AddForeignKey("dbo.Orders", "OrderPlayerId", "dbo.OrderPlayers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "OrderPlayerId", "dbo.OrderPlayers");
            DropIndex("dbo.Orders", new[] { "OrderPlayerId" });
            DropColumn("dbo.Orders", "OrderPlayerId");
            DropTable("dbo.OrderPlayers");
        }
    }
}
