namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderPlayer3 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.OrderPlayers", "OrderId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderPlayers", "OrderId", c => c.Int(nullable: false));
        }
    }
}
