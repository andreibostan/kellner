namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyOrderTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Orders", name: "ApplicationUserId", newName: "ApplicationUser_Id");
            RenameIndex(table: "dbo.Orders", name: "IX_ApplicationUserId", newName: "IX_ApplicationUser_Id");
            AddColumn("dbo.Orders", "Username", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Username");
            RenameIndex(table: "dbo.Orders", name: "IX_ApplicationUser_Id", newName: "IX_ApplicationUserId");
            RenameColumn(table: "dbo.Orders", name: "ApplicationUser_Id", newName: "ApplicationUserId");
        }
    }
}
