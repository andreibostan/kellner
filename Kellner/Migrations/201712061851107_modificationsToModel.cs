namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modificationsToModel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Categories", "Description", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Items", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Items", "Description", c => c.String(maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Items", "Description", c => c.String());
            AlterColumn("dbo.Items", "Name", c => c.String());
            AlterColumn("dbo.Categories", "Description", c => c.String());
            AlterColumn("dbo.Categories", "Name", c => c.String());
        }
    }
}
