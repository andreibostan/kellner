namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCompany : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Categories", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("dbo.Categories", "CompanyId");
            AddForeignKey("dbo.Categories", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Categories", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Categories", new[] { "CompanyId" });
            DropColumn("dbo.Categories", "CompanyId");
            DropTable("dbo.Companies");
        }
    }
}
