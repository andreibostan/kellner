namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyOrderTable2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Active", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "Action", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Action");
            DropColumn("dbo.Orders", "Active");
        }
    }
}
