namespace Kellner.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyOrderTable3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "TableName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "TableName");
        }
    }
}
