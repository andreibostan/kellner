﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kellner.Models
{
    public class Company
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Category> Categories { get; set; }

        public List<ApplicationUser> Employees { get; set; }

        public List<Table> Tables { get; set; }
    }
}