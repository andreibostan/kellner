﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kellner.Models
{
    public class Item
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The Name value cannot exceed 100 characters. ")]
        public string Name { get; set; }

        [StringLength(1000, ErrorMessage = "The Description value cannot exceed 1000 characters. ")]
        public string Description { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public double Price { get; set; }
    }
}