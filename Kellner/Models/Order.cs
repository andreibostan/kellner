﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kellner.Models
{
    public class Order
    {
        public int Id { get; set; }

        public int CompanyId { get; set; }

        public int TableId { get; set; }

        public string Username { get; set; }

        public bool Active { get; set; }

        public string Action { get; set; }

        public string TableName { get; set; }

        public int? OrderPlayerId { get; set; }

        public OrderPlayer OrderPlayer { get; set; }
    }
}