﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kellner.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }

        public int CompanyId { get; set; }

        public int TableId { get; set; }

        public string Username { get; set; }

        public bool Active { get; set; }

        public string Action { get; set; }

        public string TableName { get; set; }

        public string PlayerId { get; set; }
    }
}