﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kellner.Models
{
    public class OrderPlayer
    {
        public int Id { get; set; }

        public string PlayerId { get; set; }
    }
}