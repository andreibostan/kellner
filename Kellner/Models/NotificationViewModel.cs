﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kellner.Models
{
    public class NotificationViewModel
    {
        public bool? QueueNotifications { get; set; }

        public bool? SubscribedNotifications { get; set; }
    }
}