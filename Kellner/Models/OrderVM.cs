﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kellner.Models
{
    public class OrderVM
    {
        public string Action { get; set; }

        public bool? Unsubscribe { get; set; }
    }
}