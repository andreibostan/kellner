﻿using System;
using System.Collections.Generic;

namespace Kellner.Models
{
    public class Table
    {
        public int Id { get; set; }

        public int CompanyId { get; set; }

        public string Name { get; set; }

        public string QRCode { get; set; }

        public List<Order> Orders { get; set; }
    }
}
